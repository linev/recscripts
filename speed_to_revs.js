laps = telemetry.getAllLaps()
for (var i = 0; i < laps.length; i++)
{
    var lap = laps[i]
    var speedChannel = lap.channel("speed")
    var rpmChannel = lap.channel("rpm")
    var speedToRevsChannel = lap.editChannel("speed_to_revs")
    for (var j = 0; j < speedChannel.length; j++)
    {
        var speed = speedChannel.value(j)
        var rpm = rpmChannel.value(j)
        if (rpm < 1)
            speedToRevsChannel.setValue(j, 0);
        else
            speedToRevsChannel.setValue(j, speed / rpm);
    }
}