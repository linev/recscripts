laps = telemetry.getAllLaps()
for (var i = 0; i < laps.length; i++)
{
    var lap = laps[i]
    var throttle = lap.channel("throttle", "%")
    var brake = lap.channel("brake", "%")
    var coasting = lap.editChannel("coasting");
    for (var j = 0; j < coasting.length; j++)
    {
        coasting.setValue(j, (throttle.value(j) > 1 && brake.value(j) > 1) ? 1 : 0)
    }
}