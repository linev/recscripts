laps = telemetry.getAllLaps()
for (var i = 0; i < laps.length; i++)
{
    var lap = laps[i]
    var chargeChannel = lap.channel("energyersbattery")
    var torqueChannel = lap.channel("torquemgu_k")
    var totalChargeChannel = lap.editChannel("total_charge")
    var currentChargeChannel = lap.editChannel("current_charge")
    var charge = 0;
    var prevCharge = -1e25;
    var totalCharge = 0;
    var totalKCharge = 0;
    
    for (var j = 0; j < chargeChannel.length; j++)
    {
        charge = chargeChannel.value(j)
        if (charge > prevCharge && prevCharge > 0)
        {
            if (torqueChannel.value(j) < 0)
                totalKCharge += charge - prevCharge;                
            totalCharge += charge - prevCharge;
            currentChargeChannel.setValue(j, charge - prevCharge);
        }
        totalChargeChannel.setValue(j, totalCharge);
        prevCharge = charge;
    }
}