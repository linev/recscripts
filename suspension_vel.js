laps = telemetry.getAllLaps()
for (i = 0; i < laps.length; i++)
{
    lap = laps[i]
    suspensionTravelLFChannel = lap.channel("lfsuspensiontravel")
    suspensionTravelRFChannel = lap.channel("rfsuspensiontravel")
    suspensionTravelLRChannel = lap.channel("lrsuspensiontravel")
    suspensionTravelRRChannel = lap.channel("rrsuspensiontravel")
    lapTimeChannel = lap.channel("lapcurrentlaptime")
    size = suspensionTravelLFChannel.length
    suspensionVelLFChannel = lap.editChannel("lfshockvel")
    suspensionVelRFChannel = lap.editChannel("rfshockvel")
    suspensionVelLRChannel = lap.editChannel("lrshockvel")
    suspensionVelRRChannel = lap.editChannel("rrshockvel")
    for (j = 1; j < size; j++)
    {
        dt = lapTimeChannel.value(j) - lapTimeChannel.value(j - 1)
        if (dt == 0)
        {
            suspensionVelLFChannel.setValue(j, 0)
            suspensionVelRFChannel.setValue(j, 0)
            suspensionVelLRChannel.setValue(j, 0)
            suspensionVelRRChannel.setValue(j, 0)
        }
        else
        {
            speedLF = (suspensionTravelLFChannel.value(j) - suspensionTravelLFChannel.value(j - 1)) / dt
            speedRF = (suspensionTravelRFChannel.value(j) - suspensionTravelRFChannel.value(j - 1)) / dt
            speedLR = (suspensionTravelLRChannel.value(j) - suspensionTravelLRChannel.value(j - 1)) / dt
            speedRR = (suspensionTravelRRChannel.value(j) - suspensionTravelRRChannel.value(j - 1)) / dt
            suspensionVelLFChannel.setValue(j, speedLF)
            suspensionVelRFChannel.setValue(j, speedRF)
            suspensionVelLRChannel.setValue(j, speedLR)
            suspensionVelRRChannel.setValue(j, speedRR)
        }
    }
    suspensionVelLFChannel.setValue(0, suspensionVelLFChannel.value(1))
    suspensionVelRFChannel.setValue(0, suspensionVelLFChannel.value(1))
    suspensionVelLRChannel.setValue(0, suspensionVelLFChannel.value(1))
    suspensionVelRRChannel.setValue(0, suspensionVelLFChannel.value(1))
}