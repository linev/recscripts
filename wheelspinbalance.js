rec.calculate("$lrspeed / $rpm * 1000", "lrwheelspin")
rec.calculate("$rrspeed / $rpm * 1000", "rrwheelspin")

rec.calculate("($lfspeed + $rfspeed) / 2", "frontavgwheelspeed")
rec.calculate("($lrspeed + $rrspeed) / 2", "rearavgwheelspeed")

rec.calculate("$lfspeed - $frontavgwheelspeed", "lfwheelspeeddeltatoaverage")
rec.calculate("$rfspeed - $frontavgwheelspeed", "rfwheelspeeddeltatoaverage")
rec.calculate("$lrspeed - $rearavgwheelspeed", "lrwheelspeeddeltatoaverage")
rec.calculate("$rrspeed - $rearavgwheelspeed", "rrwheelspeeddeltatoaverage")

rec.calculate("($rearavgwheelspeed - $frontavgwheelspeed) / 2", "balance")