rec.calculate("$lfshockdefl / $vertaccel", "lfsuspensionratio");
rec.calculate("$rfshockdefl / $vertaccel", "rfsuspensionratio");
rec.calculate("$lrshockdefl / $vertaccel", "lrsuspensionratio");
rec.calculate("$rrshockdefl / $vertaccel", "rrsuspensionratio");

laps = telemetry.getAllLaps()
for (var i = 0; i < laps.length; i++)
{
    let lap = laps[i]
    let lfshockdefl = lap.channel("lfshockdefl")
    let rfshockdefl = lap.channel("rfshockdefl")
    let lrshockdefl = lap.channel("lrshockdefl")
    let rrshockdefl = lap.channel("rrshockdefl")
    let vertaccel = lap.channel("vertaccel")

    let lfsuspensionratio = lap.editChannel("lfsuspensionratio")
    let rfsuspensionratio = lap.editChannel("rfsuspensionratio")
    let lrsuspensionratio = lap.editChannel("lrsuspensionratio")
    let rrsuspensionratio = lap.editChannel("rrsuspensionratio")


    for (var j = 0; j < vertaccel.length; j++)
    {
        if (vertaccel.value(j) < 1e-3)
        {
            lfsuspensionratio.setValue(j, 0);
            rfsuspensionratio.setValue(j, 0);
            lrsuspensionratio.setValue(j, 0);
            rrsuspensionratio.setValue(j, 0);
        }
        else
        {
            lfsuspensionratio.setValue(j, lfshockdefl.value(j) / vertaccel.value(j));
            rfsuspensionratio.setValue(j, rfshockdefl.value(j) / vertaccel.value(j));
            lrsuspensionratio.setValue(j, lrshockdefl.value(j) / vertaccel.value(j));
            rrsuspensionratio.setValue(j, rrshockdefl.value(j) / vertaccel.value(j));
        }
    }
}