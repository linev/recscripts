laps = telemetry.getAllLaps()
for (var i = 0; i < laps.length; i++)
{
    var lap = laps[i]
    var speed = lap.channel("speed")
    var lfspeed =  lap.channel("lfspeed")
    var rfspeed =  lap.channel("rfspeed")
    var lrspeed =  lap.channel("lrspeed")
    var rrspeed =  lap.channel("rrspeed")

    var lflock =  lap.editChannel("lflock")
    var rflock =  lap.editChannel("rflock")
    var lrlock =  lap.editChannel("lrlock")
    var rrlock =  lap.editChannel("rrlock")


    for (var j = 0; j < speed.length; j++)
    {
        if (speed.value(j) < 1)
        {
            lflock.setValue(j, 0);
            rflock.setValue(j, 0);
            lrlock.setValue(j, 0); 
            rrlock.setValue(j, 0); 
        }
        else 
        {
            lflock.setValue(j, 1 - Math.min(lfspeed.value(j) / speed.value(j), 1));
            rflock.setValue(j, 1 - Math.min(rfspeed.value(j) / speed.value(j), 1));
            lrlock.setValue(j, 1 - Math.min(lrspeed.value(j) / speed.value(j), 1)); 
            rrlock.setValue(j, 1 - Math.min(rrspeed.value(j) / speed.value(j), 1));
        }
    }
}